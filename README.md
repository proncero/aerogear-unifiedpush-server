# Unified Push Server (UPS)
This is the documentation to run an instance of the Unified Push Server, in order to start getting push notifications.

## Configure Database
This instance is pre-configured to use POSTGRES. We have to pre-create the target database in order to be used by UPS.

```
# Connect to the database host and execute the following (values applied to test environment)
mysql> create database testups default character set = "UTF8" default collate = "utf8_general_ci";
mysql> create user 'admups'@'localhost' identified by 'admups';
mysql> GRANT SELECT,INSERT,UPDATE,ALTER,DELETE,CREATE,DROP ON testups.* TO 'admups'@'localhost';
```

## How to run Unified Push Server
In order to launch an instance of the UPS, login into Openshift and execute the following through Openshift CLI.

```
oc process -f /templates/ups.yml POSTGRES_SERVICE_HOST=xxxx POSTGRES_SERVICE_PORT=xxxx POSTGRES_DATABASE=xxxx POSTGRES_USER=xxxx POSTGRES_PASSWORD=xxxx KEYCLOAK_HOSTNAME=xxxx | oc create -f -
```

> Note:
> KEYCLOAK_HOSTNAME corresponds to the Keycloak Hostname, which normally is deployed outside UPS project. In the case of test environment, this KEYCLOAK_HOSTNAME is  test-keycloak.web.cern.ch

Aerogear will deploy in two phases:
- First one, corresponds to the database migration, performed by an init container (see template).
- Once success, it will deploy the ups war file into wildfly. If there is any error, you can see the console on Openshift.

## How it's been done
By default, production UPS instances use the stable tag on the ups ImageStream. This tag is set in the ImageStream to point to an existing version tag and indicate which version of the image to use in this deployment (there is no stable tag in the image repository in the GitLab registry).

Apply the procedure to update the UPS master image below to build an image and set the stable tag.
To use an existing image, use oc manually:

```
# import all tags - for this example we will use test-aerogear, the one from dev cluster
oc import-image -n test-aerogear ups --all
# tag one version as stable
oc tag -n test-aerogear ups:1.2.4.Final ups:stable
```

## Updating UPS
To update the template, create a new branch and modify the template. GitLab-CI will offer a manual trigger to deploy the new template to dev cluster.

When satisfied, merge changes and apply a git tag in order to automatically update the template in the prod cluster. Use tags with UPS version and incremental release number (e.g. 1.2.4.Final-1, 1.2.4.Final-2 etc when the UPS version doesn't change).

### Update master image

In order to deploy a new version of the UPS master, create a new branch and update the Dockerfile argument ```UPSVER``` to the appropriate value. Then, take a look to the original project [aerogear/dockerfiles](https://github.com/aerogear/dockerfiles), specially to the ```standalone-full-sample.xml``` configuration file. Normally, there are a few changes affecting new versions that must be put in place, othwerise won't work. Then open a merge request. The new image will be built and deployed with tag latest.

The new image can now be tested. Note that existing instances use the stable tag by default and will be not use the new image until the stable tag is updated. 

To test it in dev cluster, verify the new image by editing a test instance's DeploymentConfig to point to ups:latest (also edit the reference in the imageChangeParams part).

To validate the correct initialization of a new UPS instance, delete the pod so a new pod is recreated and the instance is re-initialized.

When satisfied:

- Tag the last commit in master with a meaningful git tag (usually with the UPS version and an incremental release number e.g. 1.2.4.Final-1). This will import the new image into the production and development clusters, with a Docker tag equals to the git tag. It will also make available a manual trigger to mark the image as stable (and redeploy existing applications).
- Run the manual triggers in the pipeline corresponding to the git tag to mark that image as stable. This will trigger update and redeployment of all instances. A configurable grace period is granted for UPS jobs to complete during redeployment.

## Links
- [Aerogear](https://aerogear.org/)
- [Aerogear repository](https://github.com/aerogear/aerogear-unifiedpush-server)
- [aerogear/dockerfiles](https://github.com/aerogear/dockerfiles)
