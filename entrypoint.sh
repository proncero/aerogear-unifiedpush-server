#!/bin/bash

# launch wildfly
echo "launching wildfly"
exec /opt/jboss/wildfly/bin/standalone.sh -Dups.realm.name=aerogear -Dups.auth.server.url=https://${KEYCLOAK_HOSTNAME} -b 0.0.0.0 -bmanagement=0.0.0.0 --server-config=standalone.xml $@