# Use latest jboss/wildfly image as the base
FROM jboss/wildfly:13.0.0.Final
LABEL maintainer="web-services-core <web-services-core@cern.ch>"

# Run everything below as root user
USER root

# Clean the metadata
RUN yum install -y unzip wget gettext && yum -q clean all

## POSTGRES
ENV psql_module_dir=$JBOSS_HOME/modules/org/postgresql/main/
ENV psql_connector_jar=postgresql-jdbc.jar
RUN mkdir -p ${psql_module_dir}
RUN wget -O ${psql_connector_jar} http://search.maven.org/remotecontent\?filepath\=org/postgresql/postgresql/42.2.2/postgresql-42.2.2.jar
RUN mv ${psql_connector_jar} ${psql_module_dir}
COPY configuration/psql-module.xml ${psql_module_dir}/module.xml

# Rename the original configuration file
RUN mv $JBOSS_HOME/standalone/configuration/standalone.xml $JBOSS_HOME/standalone/configuration/standalone.xml.orig

# WildFly configuration file ready for HTTPS
ADD configuration/standalone-full-sample.xml $JBOSS_HOME/standalone/configuration/standalone.xml

# Switch to the working dir /opt/jboss/wildfly
WORKDIR /opt/jboss/wildfly

# Download Aerogear distribution
ENV UPSVER 2.1.0.Final
ENV UPSDIST=/opt/aerogear-unifiedpush-server-${UPSVER}

RUN curl -L -o /opt/aerogear-unifiedpush-server-${UPSVER}-dist.tar.gz https://github.com/aerogear/aerogear-unifiedpush-server/releases/download/${UPSVER}/aerogear-unifiedpush-server-${UPSVER}-dist.tar.gz
WORKDIR /opt
RUN tar zxf aerogear-unifiedpush-server-${UPSVER}-dist.tar.gz

# unzip migrator. liquibase.properties will be injected at running time.
WORKDIR $UPSDIST/migrator
RUN unzip ups-migrator-dist.zip

# copy and run startup script
# migration is done inside the startup script before launching the server
ADD ["entrypoint.sh","init-dbmigration.sh","/opt/"]
RUN chmod +x /opt/entrypoint.sh /opt/init-dbmigration.sh

# Run everything below as aerogear user
USER jboss

# Switch to the working dir $JBOSS_HOME/standalone/deployments
WORKDIR /opt/jboss/wildfly/standalone/deployments

# copy war files
RUN cp $UPSDIST/servers/unifiedpush-server-wildfly.war $JBOSS_HOME/standalone/deployments

ENTRYPOINT ["/bin/bash", "/opt/entrypoint.sh"]

# Expose default port
EXPOSE 8080