#!/bin/bash
set -e
# run migrator
echo "Starting Liquibase migration"
cd $UPSDIST/migrator/ups-migrator
./bin/ups-migrator --url=jdbc:postgresql://${POSTGRES_SERVICE_HOST}:${POSTGRES_SERVICE_PORT}/${POSTGRES_DATABASE} --driver=org.postgresql.Driver --username=${POSTGRES_USER} --password=${POSTGRES_PASSWORD} --changeLogFile=liquibase/master.xml --logLevel=INFO update